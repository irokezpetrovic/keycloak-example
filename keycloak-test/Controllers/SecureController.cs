using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace keycloak_test.Controllers
{
    [Controller]
    public class SecureController
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public SecureController(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        [HttpGet("/foo")]
        public string Foo()
        {
            return "Foo!";
        }
        
        [HttpGet("/bar")]
        [Authorize(Roles="Administrator")]
        
        public string Bar()
        {
            return "Bar!";
        }

        [HttpGet("/bar/{barId}")]
        public string Bar([FromRoute] string barId)
        {
            var isPublisher = _contextAccessor.HttpContext.User.IsInRole(Security.ROLE_PUBLISHER);
            var isInRole = _contextAccessor.HttpContext.User.HasClaim("user_roles", Security.ROLE_PRODUCTION_PUBLISHER);
            //var isInRole = _contextAccessor.HttpContext.User.IsInRole(Security.ROLE_PRODUCTION_PUBLISHER);
            return $"Bar: {barId}, isProductionPublisher = {isInRole}";
        }
        
    }
}