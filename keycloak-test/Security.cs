namespace keycloak_test
{
    public class Security
    {
        public const string ROLE_PUBLISHER = "publisher";
        public const string ROLE_PRODUCTION_PUBLISHER = "production-publisher";
    }
}