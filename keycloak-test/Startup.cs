﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace keycloak_test
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        private readonly IHostingEnvironment _hostingEnvironment;

        public Startup(IHostingEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _hostingEnvironment = hostingEnvironment;
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllOrigin",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyOrigin().AllowCredentials().AllowAnyMethod());
            });

            
            services.Configure<IdentityOptions>(options =>
            {
                //options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                //options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = "user_roles";                
            });
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                
            }).AddJwtBearer(o =>
            {
                //o.Authority = _configuration["Jwt:Authority"];
                o.Authority = "http://localhost:8080/auth/realms/master";
                //o.Audience = _configuration["Jwt:Audience"];
                o.Audience = "Ingate-realm";

                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = new TokenValidationParameters
                {
                    RoleClaimType = "user_roles"
                };
                
                          
                
                o.Events = new JwtBearerEvents()
                {                    
                    OnAuthenticationFailed = c =>
                    {
                        c.NoResult();

                        c.Response.StatusCode = 500;
                        c.Response.ContentType = "text/plain";

                        return c.Response.WriteAsync(_hostingEnvironment.IsDevelopment()
                            ? c.Exception.ToString()
                            : "An error occured processing your authentication.");
                    }
                };
            });

            
            services.AddAuthorization(options =>
            {                
                options.AddPolicy(Security.ROLE_PUBLISHER, policy => policy.RequireClaim("user_roles", Security.ROLE_PUBLISHER));
                options.AddPolicy("Administrator",
                    policy => policy.RequireClaim("user_roles", "Administrator"));
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc();
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAllOrigin"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            /*
            app.Use((ctx, next) =>
            {
                ctx.Response.Headers.Add("Access-Control-Allow-Origin","http://localhost:8089");
                ctx.Response.Headers.Add("Access-Control-Allow-Credentials","true");
                ctx.Response.Headers.Add("Access-Control-Allow-Headers","X-Requested-With");
                return next.Invoke();
            });
            */
            app.UseCors("AllowAllOrigin");
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}