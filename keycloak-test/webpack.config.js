const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlPlugin = require('html-webpack-plugin');
let devMode = process.env.NODE_ENV !== 'production';



const plugins = [
    new webpack.NamedModulesPlugin(),
    new HtmlPlugin({
        title: 'App',
        chunks: ['app'],
        filename: 'app.html',
        template: path.join(__dirname, 'wwwsrc', 'app.html')
    }),
    new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].css'
        //chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
    new webpack.ProvidePlugin({
        React: 'react',
        ReactDom: 'react-dom'
    })
];
module.exports = {
    entry: {
        app: './wwwsrc/app.js',
        vendor: ['react', 'react-dom']
    },
    module: {
        rules: [{
                test: /\.(js)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    //presets: ['es2015', 'react', 'stage-2', 'stage-0'],
                    presets:["@babel/preset-env","@babel/preset-react"],
                    //plugins:['transform-decorators']
                    //plugins:["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }]
                }
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.(_css)$/,
                use:[devMode?'style-loader':MiniCssExtractPlugin.loader,'css-loader','postcss-loader']
            },
            {
                test: /\.(scss|css)$/,
                use: [devMode?'style-loader':MiniCssExtractPlugin.loader, { loader: 'css-loader', options: { modules: true, sourceMap: true, localIdentName: '[name]__[local]--[hash:base64:8]' } }, 'postcss-loader']
            },
            {
                test: /\.(svg|woff|woff2|ttf|eot)$/,
                use: 'file-loader'
            }]
    },

    resolve: {
        extensions: ['.js', '.htmls', '.css', '.scss'],
        modules: [             
            path.resolve(__dirname,"wwwsrc"),
            "node_modules"
        ]
    },
    plugins: plugins,
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'wwwroot'),
        pathinfo: true
    },
    devServer: {
        host: '0.0.0.0',
        port: 8089,
        contentBase: [path.join(__dirname, './wwwroot/')],
        disableHostCheck: true
    }

};