import {transport, request} from 'popsicle';

let apiUrl = "http://localhost:5000";

let _token = '';

function SetToken(token) {
    _token = token;
}

function JsonGet(options) {

    let requestOptions = options;
    if (typeof options === "string") {
        requestOptions = {
            method: 'GET',
            url: apiUrl + options,
            headers: {
                "Authorization": `Bearer ${_token}`
            }
        }
    }

    var req = request(apiUrl + options);
    req.headers.append("Authorization",`Bearer ${_token}`);
    //console.log(req);
    return transport()(req)
        .then(resp=>{
            return resp.body.rawBody;
        });
    /*
        .then(resp=>{
            return resp.body;
        });
        */
}


function Foo() {
    return JsonGet('/foo');
}

function Bar() {
    return JsonGet('/bar');
}

function BarWithId(id) {
    return JsonGet(`/bar/${id}`);
}

export default {
    Foo,
    Bar,
    BarWithId,
    SetToken

}