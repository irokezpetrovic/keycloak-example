import Keycloak from 'keycloak-js';
import Api from './api';



class App extends React.Component {
    constructor() {
        super();
        this.state = {
            cloak:null
        };
        let keycloak = new Keycloak({
            "realm": "master",
            "url": "http://localhost:8080/auth",
            //"ssl-required": "external",
            //"resource": "bookBox",
            "public-client": true,
            "clientId": "depo"
        });
        
        keycloak.init({onLoad: 'login-required', flow: 'implicit'})
            .success((authenticated)=> {
                //console.log(authenticated ? 'authenticated' : 'not authenticated');
                //console.log(keycloak.token);
                this.setState({cloak:keycloak});
                if (authenticated){
                    Api.SetToken(keycloak.token);
                    
                    keycloak.loadUserInfo().success((info)=>{
                        console.log(info);
                    })    
                }
            }).error(function () {
            alert('failed to initialize');
        });
    }

    
    onFoo(){
        Api.Foo()
            .then(console.log);         
    };
    
    onBar(){
        Api.Bar()
            .then(console.log);
    };
    onBarId(){
        Api.BarWithId(10)
            .then(console.log);
    };
    render() {
        return <div>
            <h1>App</h1>
            <button onClick={this.onFoo}>Foo</button>
            <button onClick={this.onBar}>Bar</button>
            <button onClick={this.onBarId}>Bar Id</button>
        </div>;
    }
}

ReactDom.render(<App/>, document.getElementById('root'));